$('#owl-team').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    center:true,
    pagination: false,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:6
        }
    },
    navigationText: ["◀ Left <strong>arrow</strong>","Right <strong> arrow </strong> ▶"]
})

var owl = $('#owl-team');
owl.owlCarousel();

$('#team-prev').click(function() {    
    owl.trigger('prev.owl.carousel');
    return false;
})
$('#team-next').click(function() {    
    owl.trigger('next.owl.carousel');
    return false;
})