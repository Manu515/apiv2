require('dotenv').config();
const express = require('express');
const PORT = 8080;
const app = express();
const path = require('path');
const sendMail = require('./mail.js');
const pdfgen = require('./pedfgen')
var reload = require('express-reload')


app.use(express.static(__dirname + '/views/public'));
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'views/public', 'index.html'));
});

app.use(express.urlencoded({
    extended: false
}));
app.use(express.json());


app.post('/email', (req, res) => {
    const gen = pdfgen.generarPDF(req.body);
    const { persona, email, pic, vid, dronepic, dronevid, drone360pic, drone360vid, mapping } = req.body;
    /* console.log('data: ', req.body); */
    sendMail(persona, email, pic, vid, dronepic, dronevid, drone360pic, drone360vid, mapping, function (err, data) {
        if (err) {
            res.status(500).json({ message: 'error intenro' });
        } else {
            
            res.json({ message: 'mensaje enviadoooooooooo' })
            
        }
        process.once("SIGHUP", function () {
            reloadSomeConfiguration();
          })
    });

});


app.post('/contact', (req,res)  =>{
    const { email,name,subjectn,message } = req.body
    console.log('data: ', req.body);
}) 


app.listen(PORT, () => {
    console.log('SERVIDOR CORRIENDO EN localhost:' + PORT);
})