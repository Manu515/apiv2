require('dotenv').config();
const nodemailer = require('nodemailer');
const mailgun = require('nodemailer-mailgun-transport');

const auth = {
    auth: {
        api_key: process.env.API_KEY,
        domain: process.env.DOMAIN,
    }
};

const transporter = nodemailer.createTransport(mailgun(auth));

const sendMail = (persona, email, pic, vid, dronepic, dronevid, drone360pic, drone360vid, mapping, cb) => {
    const mailOptions = {
        from: email,
        to: process.env.TO,
        subjet: persona,
        html: '<br><h1>Cliente: </h1>'+ persona + ' <br><h1>email: </h1>' + email + '<br> <h3>Fotos360: </h3>'+pic + ' <br><h3>Videos360: </h3>' + vid + ' <br><h3>Fotos drone: </h3>'+dronepic + '<br> <h3>Video Drone: </h3>'+dronevid + ' <br><h3>Foto 360 Drone: </h3>'+drone360pic + ' <br><h3>Drone 360 Video: </h3>'+drone360vid + ' <br><h3>Mapping: </h3>' +mapping,
        attachment:[{
            filename: 'mipdf.pdf',
            path: './mipdf.pdf',
            contentType: 'application/pdf'
        }]

    };

    transporter.sendMail(mailOptions, function (err, data) {
        if (err) {
            cb(err, null)
        } else {
            cb(null, data);
        }

    });
}
module.exports = sendMail;

